import React from 'react';
import { Card, Container } from 'react-bootstrap';
import FileUpload from './FileUpload';

const FileUploadView = () => {
    const zipFileIcon = <i className="fas fa-file-archive mr-3"></i>;
    return(
        <Container style={{ maxWidth: 550 }}>    
            <Card style={{ marginTop: 20 }}>
                <Card.Header>
                    {zipFileIcon} File Upload
                </Card.Header>
                <Card.Body>
                    <FileUpload/>
                </Card.Body>
            </Card>
        </Container>
    );
};

export default FileUploadView;