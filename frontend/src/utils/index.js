export const updateUploadPercentage = setUploadPercentageCb => progressEvent => {
    const percentage = Math.round(
        (progressEvent.loaded * 100) / progressEvent.total
    );
    setUploadPercentageCb(parseInt(percentage));
};