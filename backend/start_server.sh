#!/bin/bash

source ./venv/bin/activate

export LD_LIBRARY_PATH=/usr/local/lib/wrap_lem:$LD_LIBRARY_PATH
ldconfig

export FLASK_APP=app
export FLASK_MODE=development

flask run
