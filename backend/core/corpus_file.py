from dataclasses import dataclass


@dataclass
class Corpus():
    uuid: str
    cwd: str
    path: str
    file: str
    email: str = None
    meta: dict = None
    categories: dict = None
