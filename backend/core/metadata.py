import json
import datetime as dt
import openpyxl as xlsx
from logger import logger


def load_xlsx_meta(filename):
    wb = xlsx.load_workbook(filename=filename)
    ws = wb.worksheets[0]
    keys = get_attributes_xlsx(ws)
    files = get_attributes_xlsx(ws, row=False)
    if keys is not None:
        metadata = {}
        categories = {key: [] for key in keys}
        for row, filename in enumerate(files, 2):
            row_values = []
            for column in enumerate(keys, 2):
                row_values.append(ws.cell(column=column[0], row=row).value)
            _meta = dict(zip(keys, row_values))
            metadata[filename] = {
                key: value
                for key, value in _meta.items() if value is not None
            }
            datetime_to_str(metadata[filename])
            append_catergoies(metadata, categories)
        unique_categories(categories)
        return metadata, categories


def unique_categories(categories):
    for key in categories.keys():
        categories[key] = list(set(categories[key]))


def append_catergoies(_dict, categories):
    for item in _dict.values():
        for key, value in item.items():
            if value is not None:
                categories[key].append(value)


def get_attributes_xlsx(ws, row=True):
    attrs = []
    counter = 1
    attr = None
    while counter != 0:
        if row:
            attr = ws.cell(column=counter, row=1).value
        else:
            attr = ws.cell(column=1, row=counter).value
        if attr is not None:
            counter += 1
            attrs.append(attr)
        else:
            counter = 0
    if (len(attrs) > 1 and attrs[0].lower() == 'filename'):
        return attrs[1:]
    raise KeyError("Worksheet cell(1,1) must contain key 'filename'")


def datetime_to_str(metadata):
    if 'publication_date' in metadata:
        date = metadata['publication_date']
        if isinstance(date, dt.datetime):
            metadata['publication_date'] = date.strftime('%Y-%m-%d')


def load_json_meta(filename):
    metadata = None
    with open(filename, 'r') as jsonfile:
        metadata = json.load(jsonfile)
    metadata = metadata['metadata']
    table = [[], []]
    for item in metadata:
        table[0].append(item['name'])
        table[1].append(item['value'])
    metadata = dict(zip(table[0], table[1]))
    return metadata, None


def load_metadata(filename):
    data = None
    categories = None
    extension = filename.split('.')[-2:]
    try:
        if (extension[0] == 'meta' and extension[1] in ['xlsx', 'json']):
            if extension[1] == 'xlsx':
                data, categories = load_xlsx_meta(filename)
            elif extension[1] == 'json':
                data, categories = load_json_meta(filename)
    except FileNotFoundError as fnf:
        logger.error(fnf)
    except KeyError as e:
        logger.error("Filename: %s %s" % (filename, e))
    return data, categories
