import os
import sys
import shutil
import pytest
import pathlib
import zipfile

sys.path.insert(0, str(pathlib.Path(__file__).absolute().parent.parent))  # noqa

import app
from constants import UPLOAD_FOLDER


@pytest.fixture
def client():
    _app = app.create_app()
    _app.config['TESTING'] = True
    with _app.test_client() as client:
        app.app_init()
        yield client
    remove_dir_contents(UPLOAD_FOLDER)


@pytest.fixture
def zipfile_payload(tmp_path):
    filename = str(tmp_path / 'test.zip')
    with zipfile.ZipFile(filename, mode='w') as zf:
        zf.writestr(filename, 'text')
    with open(filename, 'rb') as file:
        yield file, 'test.zip'


def remove_dir_contents(dir_path):
    for root, dirs, files in os.walk(dir_path):
        for f in files:
            os.unlink(os.path.join(root, f))
        for d in dirs:
            shutil.rmtree(os.path.join(root, d))
