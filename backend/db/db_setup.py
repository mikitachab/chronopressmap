import mongoengine

from constants import (
    MONGO_DB_ALIAS,
    MONGO_DB_NAME,
    MONGO_DB_HOST,
    MONGO_DB_PORT,
)


def db_init():
    mongoengine.register_connection(
        alias=MONGO_DB_ALIAS,
        name=MONGO_DB_NAME,
        host=MONGO_DB_HOST,
        port=MONGO_DB_PORT
    )
