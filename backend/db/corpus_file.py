from mongoengine import (
    EmbeddedDocument,
    EmbeddedDocumentListField,
    StringField,
    DictField,
)

from .location import Location


class CorpusFile(EmbeddedDocument):
    filename = StringField(required=True)
    locations = EmbeddedDocumentListField(Location)
    categories = DictField(required=True)
