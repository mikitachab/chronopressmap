import logging


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def logger_init(logfile_path):
    formatter = logging.Formatter('%(levelname)s %(asctime)s: %(message)s', '%Y-%m-%d %H:%M:%S')
    for handler in (logging.StreamHandler(), logging.FileHandler(logfile_path, 'a', encoding='utf-8', delay='true')):
        handler.setLevel(logging.DEBUG)
        handler.setFormatter(formatter)
        logger.addHandler(handler)
