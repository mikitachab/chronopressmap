from utils import zip_utils

from email_validator import validate_email, EmailNotValidError


def email_type(email_str):
    try:
        v = validate_email(email_str)
        return v['email']
    except EmailNotValidError as e:
        raise ValueError(e)


def zipfile_type(file):
    if zip_utils.validate_zip_format(file.stream._file):
        file.stream.seek(0)
        return file
    raise ValueError('File is not valid ZIP archive')
