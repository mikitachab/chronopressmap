from geo_location.geo_code.geo_code_access_interface import GeoCodeAccessInterface
from geo_location.geo_code.geo_names_access import GeoNamesAccess
from geo_location.geo_code.nominatim_access import NominatimAccess


class GeoCodeAccessFactory(object):
    GEO_ACCESS_MAP = {'nominatim': NominatimAccess,
                      'geo_names': GeoNamesAccess}

    def make(self, config) -> GeoCodeAccessInterface:
        geo_code = config['geoCode']
        geo_code_access = geo_code['geoCodeAccess']
        if geo_code_access not in GeoCodeAccessFactory.GEO_ACCESS_MAP:
            raise RuntimeError(f'Invalid configuration: Unknown geoCodeAccess: {geo_code_access}')
        max_rows = geo_code['maxRows']
        return GeoCodeAccessFactory.GEO_ACCESS_MAP[geo_code_access](max_rows)
